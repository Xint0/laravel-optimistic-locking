<?php

namespace Reshadman\OptimisticLocking\Tests;

use Reshadman\OptimisticLocking\StaleModelLockingException;

class OptimisticLockingTest extends TestCase
{
    public function test_throws_exception_on_concurrent_change(): void
    {
        /** @var StubLockablePostModel $truth */
        $truth = StubLockablePostModel::query()->create([
            'title' => 'Before Update Title.',
            'description' => 'Before Update Description.'
        ]);

        /** @var StubLockablePostModel $first */
        $first = StubLockablePostModel::query()->find($truth->id);
        /** @var StubLockablePostModel $second */
        $second = StubLockablePostModel::query()->find($truth->id);

        $this->expectException(StaleModelLockingException::class);

        $first->title = $wantedTitle = 'Title changed by first process.';
        $this->assertTrue($first->save());

        try {
            $second->title = 'Title changed by second process.';
            $second->save();
        } catch (StaleModelLockingException $e) {
            /** @var StubLockablePostModel $fetchedAfterFirstUpdate */
            $fetchedAfterFirstUpdate = StubLockablePostModel::query()->find($truth->id);
            $this->assertEquals($fetchedAfterFirstUpdate->title, $wantedTitle);
            $this->assertEquals($fetchedAfterFirstUpdate->lock_version, $first->lock_version);
            $this->assertEquals($fetchedAfterFirstUpdate->lock_version, $truth->lock_version + 1);

            throw $e;
        }
    }

    public function test_throws_exception_on_concurrent_delete(): void
    {
        /** @var StubLockablePostModel $original */
        $original = StubLockablePostModel::query()->create([
            'title' => 'Original title',
            'description' => 'Original description',
        ]);
        /** @var StubLockablePostModel $updated */
        $updated = StubLockablePostModel::query()->find($original->id);
        $updated->update([
            'description' => 'Updated description',
        ]);

        self::assertEquals(1, $original->lock_version);
        self::assertEquals(2, $updated->lock_version);

        $this->expectException(StaleModelLockingException::class);

        $original->delete();
    }

    public function test_does_not_throw_when_locking_disabled(): void
    {
        /** @var StubLockablePostModel $truth */
        $truth = StubLockablePostModel::query()->create([
            'title' => 'Before Update Title',
            'description' => 'Before Update Description.',
        ]);

        /** @var StubLockablePostModel $first */
        $first = StubLockablePostModel::query()->find($truth->id);
        /** @var StubLockablePostModel $second */
        $second = StubLockablePostModel::query()->find($truth->id);

        $first->title = $wantedTitle = 'Title changed by first process.';
        $this->assertTrue($first->save());

        $second->disableLocking();
        $second->description = $wantedDescription = 'Description changed by second process.';
        $this->assertTrue($second->save());

        /** @var StubLockablePostModel $fetchedAfterUpdates */
        $fetchedAfterUpdates = StubLockablePostModel::query()->find($truth->id);
        $this->assertEquals($wantedTitle, $fetchedAfterUpdates->title);
        $this->assertEquals($wantedDescription, $fetchedAfterUpdates->description);
        $this->assertEquals($truth->lock_version + 1, $first->lock_version);
        $this->assertEquals($truth->lock_version + 1, $second->lock_version);
        $this->assertEquals($truth->lock_version + 1, $fetchedAfterUpdates->lock_version);
    }

    public function test_does_not_throw_on_concurrent_delete_when_locking_disabled(): void
    {
        /** @var StubLockablePostModel $original */
        $original = StubLockablePostModel::query()->create([
            'title' => 'Original title',
            'description' => 'Original description',
        ]);
        /** @var StubLockablePostModel $updated */
        $updated = StubLockablePostModel::query()->find($original->id);
        $updated->update([
            'description' => 'Updated description',
        ]);
        $original->disableLocking();

        self::assertEquals(1, $original->lock_version);
        self::assertEquals(2, $updated->lock_version);

        self::assertTrue($original->delete());
    }

    public function test_lockingEnabled_returns_expected_result(): void
    {
        /** @var StubLockablePostModel $model */
        $model = StubLockablePostModel::query()->create([
            'title' => 'Title',
            'description' => 'Description',
        ]);

        $this->assertTrue($model->lockingEnabled());
        $model->disableLocking();
        $this->assertFalse($model->lockingEnabled());
        $model->enableLocking();
        $this->assertTrue($model->lockingEnabled());
    }

    public function test_does_not_save_when_updating_event_returns_false(): void
    {
        StubLockablePostModel::updating(static function () {
            return false;
        });

        /** @var StubLockablePostModel $model */
        $model = StubLockablePostModel::query()->create([
            'title' => 'Title',
            'description' => 'Description',
        ]);

        $model->title = 'Updated title';
        $this->assertFalse($model->save());
    }
}