<?php

return [
    'target_php_version' => null,
    'directory_list' => [
        'src',
        'Tests',
        'vendor/laravel/framework/src/Illuminate',
        'vendor/orchestra/testbench-core',
        'vendor/phpunit/phpunit/src/Framework',
    ],
    'exclude_analysis_directory_list' => [
        'vendor/',
    ],
];